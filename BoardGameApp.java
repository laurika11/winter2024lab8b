import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Hi, welcome to the game.");
		Board board = new Board();
		int numCastles = 7;
		int turns = 0;
		
		while(numCastles > 0 && turns < 8){
			System.out.println(board.toString());
			System.out.println("Number of castles: " + numCastles);
			System.out.println("Number of turns: " + turns);
			
			System.out.println("Enter a row:");
			int row = reader.nextInt();
			System.out.println("Enter a column:");
			int col = reader.nextInt();
			
			int placeToken = board.placeToken(row, col);
			while(placeToken < 0){				
				System.out.println("Invalid place. Enter a row: ");
				row = reader.nextInt();
				System.out.println("Enter a column: ");
				col = reader.nextInt();
				placeToken = board.placeToken(row, col);
			}
			
			if(placeToken == 1){
				System.out.println("There was a wall at that position.");
			}
			else{
				System.out.println("A castle tile was successfully placed.");
				numCastles--;
			}
			turns++;
		}
		
		System.out.println(board.toString());
		if(numCastles == 0){
			System.out.println("You won! :)");
		}
		else{
			System.out.println("You lost. :(");
		}
	}
}