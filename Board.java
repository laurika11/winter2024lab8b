import java.util.Random;

public class Board{
	private Tile[][] grid;
	private final int number;
	
	//Constructor method
	public Board(){
		this.number = 5;
		this.grid = new Tile[number][number];
		Tile blank = Tile.BLANK;
		Tile hiddenWall = Tile.HIDDEN_WALL;
		Random rand = new Random();
		
		for(int i = 0; i < grid.length; i++){
			int randomIndex = rand.nextInt(grid[i].length);
			for(int j = 0; j < grid[i].length; j++){
				if(j == randomIndex){
					grid[i][j] = hiddenWall;
				}
				else{
					grid[i][j] = blank;
				}
			}
		}
	}
	
	//Turns the grid/tiles into a string
	public String toString(){
		String result = "";
		
		for(int i = 0; i < grid.length; i++){
			for(int j = 0; j < grid[i].length; j++){
				result += grid[i][j].getName() + " ";
			}
			result+="\n";
		}
		return result;
	}
	
	//If the player chosed a place that already has a token or is bigger than the board, it returns false.
	//If the player choses a place that has a blank, it returns true. 
	public int placeToken(int row, int col){
		if(row >= this.number || col >= this.number || row < 0 || col < 0){
			return -2;
		}
		else if(grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL){
			return -1;
		}
		else if(grid[row][col] == Tile.HIDDEN_WALL){
			grid[row][col] = Tile.WALL;
			return 1;
		}
		else{
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
}